FROM jupyter/notebook:latest


# adding packages.
RUN pip2 install --no-cache-dir jupyter_contrib_nbextensions jupyter_nbextensions_configurator \
&&  pip3 install --no-cache-dir jupyter_contrib_nbextensions jupyter_nbextensions_configurator

RUN jupyter contrib nbextension install --user \
&& jupyter nbextensions_configurator enable --user
